﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using CardAPI.Repositories;
using CardAPI.Models;
using CardAPI.Framework;

namespace CardAPI.Tests
{
    [TestClass]
    public class ValidationControllerTest
    {
        private Controllers.ValidationController validationController;
        private IDataRepository dataRepositoryMock;
        private string cardNo;
        private Card result;
        private Card expected;

        [TestInitialize]
        public void Initialize()
        {
            dataRepositoryMock = new DataRepositoryMock();
            validationController = new Controllers.ValidationController(dataRepositoryMock);
        }

        [TestMethod]
        public void ValidVisa()
        {
            // Setup
            cardNo = "4890890348503940";
            expected = new Card() { Validation = Constants.Valid, CardType = Enums.CardType.Visa.ToString() };

            // Act
            result = validationController.Get(cardNo);

            // Assert
            Assert.AreEqual(result.Validation, expected.Validation);
            Assert.AreEqual(result.CardType, expected.CardType);
        }

        [TestMethod]
        public void ValidMasterCard()
        {
            // Setup
            cardNo = "5789023809234830";
            expected = new Card() { Validation = Constants.Valid, CardType = Enums.CardType.MasterCard.ToString() };

            // Act
            result = validationController.Get(cardNo);

            // Assert
            Assert.AreEqual(result.Validation, expected.Validation);
            Assert.AreEqual(result.CardType, expected.CardType);
        }

        [TestMethod]
        public void ValidAmex()
        {
            // Setup
            cardNo = "378209831092292";
            expected = new Card() { Validation = Constants.Valid, CardType = Enums.CardType.Amex.ToString() };

            // Act
            result = validationController.Get(cardNo);

            // Assert
            Assert.AreEqual(result.Validation, expected.Validation);
            Assert.AreEqual(result.CardType, expected.CardType);
        }

        [TestMethod]
        public void ValidJCB()
        {
            // Setup
            cardNo = "3830489593048590";
            expected = new Card() { Validation = Constants.Valid, CardType = Enums.CardType.JCB.ToString() };

            // Act
            result = validationController.Get(cardNo);

            // Assert
            Assert.AreEqual(result.Validation, expected.Validation);
            Assert.AreEqual(result.CardType, expected.CardType);
        }

        [TestMethod]
        public void InValidVisa()
        {
            // Setup
            cardNo = "4320829379847340";
            expected = new Card() { Validation = Constants.Invalid, CardType = Enums.CardType.Visa.ToString() };

            // Act
            result = validationController.Get(cardNo);

            // Assert
            Assert.AreEqual(result.Validation, expected.Validation);
            Assert.AreEqual(result.CardType, expected.CardType);
        }

        [TestMethod]
        public void InvalidMasterCard()
        {
            // Setup
            cardNo = "5237890428309480";
            expected = new Card() { Validation = Constants.Invalid, CardType = Enums.CardType.MasterCard.ToString() };

            // Act
            result = validationController.Get(cardNo);

            // Assert
            Assert.AreEqual(result.Validation, expected.Validation);
            Assert.AreEqual(result.CardType, expected.CardType);
        }

        [TestMethod]
        public void InvalidAmex()
        {
            // Setup
            cardNo = "378209810922933";
            expected = new Card() { Validation = "Does not exist", CardType = string.Empty };

            // Act
            result = validationController.Get(cardNo);

            // Assert
            Assert.AreEqual(result.Validation, expected.Validation);
            Assert.AreEqual(result.CardType, expected.CardType);
        }

        [TestMethod]
        public void InvalidJCB()
        {
            // Setup
            cardNo = "3489483405830492";
            expected = new Card() { Validation = "Does not exist", CardType = string.Empty };

            // Act
            result = validationController.Get(cardNo);

            // Assert
            Assert.AreEqual(result.Validation, expected.Validation);
            Assert.AreEqual(result.CardType, expected.CardType);
        }

        [TestMethod]
        public void DoesNotExist()
        {
            // Setup
            cardNo = "00223342890343940";
            expected = new Card() { Validation = "Does not exist", CardType = string.Empty };

            // Act
            result = validationController.Get(cardNo);

            // Assert
            Assert.AreEqual(result.Validation, expected.Validation);
            Assert.AreEqual(result.CardType, expected.CardType);
        }
    }
}
