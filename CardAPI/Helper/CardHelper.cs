﻿using CardAPI.Framework;
using System;

namespace CardAPI.Helper
{
    public static class CardHelper
    {
        public static string Validate(string ID, DateTime expiryDate)
        {
            // invalid ID length
            if (ID.Length < 15 || ID.Length > 16) return Constants.Invalid;

            // valid Amex
            if (ID.Trim().Length == 15)
            {
                return (ID[0] == '3') ? Constants.Valid : Constants.Invalid;
            }
            else
            {
                // Visa
                if (ID[0] == '4' && DateTime.IsLeapYear(expiryDate.Year)) return Constants.Valid;
                // MasterCard
                else if (ID[0] == '5' && IsPrime(expiryDate.Year)) return Constants.Valid;
                // JCB
                else if (ID[0] == '3') return Constants.Valid;
            }

            return Constants.Invalid;
        }

        public static Enums.CardType? GetCardType(string ID)
        {
            if (ID[0] == '4' && ID.Length == 16) return Enums.CardType.Visa;
            else if (ID[0] == '5' && ID.Length == 16) return Enums.CardType.MasterCard;
            else if (ID[0] == '3' && ID.Length == 15) return Enums.CardType.Amex;
            else if (ID[0] == '3' && ID.Length == 16) return Enums.CardType.JCB;
            else if (ID.Length == 16) return Enums.CardType.Unknown;

            return null;
        }

        private static bool IsPrime(int number)
        {
            if ((number & 1) == 0)
            {
                return (number == 2) ? true : false;
            }
            
            for (int i = 3; (i * i) <= number; i += 2)
            {
                if ((number % i) == 0) return false;
            }

            return number != 1;
        }
    }
}