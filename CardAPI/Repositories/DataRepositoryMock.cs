﻿using CardAPI.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CardAPI
{
    public class DataRepositoryMock: IDataRepository
    {
        static readonly DataRepositoryMock _dataRepository = new DataRepositoryMock();
        private List<CardInfo> cardInfos;
        public object Stubs { get; private set; }

        public DataRepositoryMock()
        {
            cardInfos = CardStub.GetCards();
        }

        public static DataRepositoryMock Instance
        {
            get
            {
                return _dataRepository;
            }
        }

        public DateTime? CardExists(string number)
        {
            return cardInfos.Where(x => x.CardNo == number).Select(y => y.ExpiryDate).FirstOrDefault();
        }
    }
}