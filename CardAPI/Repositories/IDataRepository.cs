﻿using System;

namespace CardAPI.Repositories
{
    public interface IDataRepository
    {
        DateTime? CardExists(string number);
    }
}
