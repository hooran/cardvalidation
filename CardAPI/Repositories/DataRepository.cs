﻿using CardAPI.Repositories;
using System;
using System.Linq;

namespace CardAPI
{
    public class DataRepository: IDataRepository
    {
        static readonly DataRepository _dataRepository = new DataRepository();
        private CardsEntities cardEntities;

        public DataRepository()
        {
            cardEntities = new CardsEntities();
        }

        public static DataRepository Instance
        {
            get
            {
                return _dataRepository;
            }
        }

        public DateTime? CardExists(string number)
        {
            var result = Instance.cardEntities.CardExists(number);
            return result.FirstOrDefault();
        }
    }
}