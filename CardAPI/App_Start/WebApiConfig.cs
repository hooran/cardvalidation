﻿using CardAPI.Helper;
using CardAPI.Repositories;
using System.Net.Http.Headers;
using System.Web.Http;
using Unity;

namespace CardAPI
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            config.Formatters.JsonFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/html"));

            var container = new UnityContainer();
            container.RegisterType<IDataRepository, DataRepository>();
            config.DependencyResolver = new UnityResolverHelper(container);
        }
    }
}
