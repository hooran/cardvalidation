﻿namespace CardAPI.Framework
{
    public class Enums
    {
        public enum CardType
        {
            Visa,
            MasterCard,
            Amex,
            JCB,
            Unknown
        }
    }
}