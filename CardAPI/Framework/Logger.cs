﻿using System;

namespace CardAPI.Framework
{
    public static class Logger
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public static void Error(Exception ex)
        {
            log.Error(ex);
        }
    }
}