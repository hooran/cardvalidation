﻿using System;
using System.Collections.Generic;

namespace CardAPI
{
    public static class CardStub
    {
        private static List<CardInfo> cards = new List<CardInfo>();
        public static List<CardInfo> GetCards()
        {
            cards.Add(new CardInfo() { ID = 1, CardNo = "1234567890123450", ExpiryDate = Convert.ToDateTime("02/02/2020") });
            cards.Add(new CardInfo() { ID = 2, CardNo = "1278934723430240", ExpiryDate = Convert.ToDateTime("02/05/2023") });
            cards.Add(new CardInfo() { ID = 3, CardNo = "2349087834984030", ExpiryDate = Convert.ToDateTime("02/02/2024") });
            cards.Add(new CardInfo() { ID = 4, CardNo = "3830489593048590", ExpiryDate = Convert.ToDateTime("02/02/2026") });
            cards.Add(new CardInfo() { ID = 5, CardNo = "3489483405830490", ExpiryDate = Convert.ToDateTime("02/02/2020") });
            cards.Add(new CardInfo() { ID = 6, CardNo = "378209831092292", ExpiryDate = Convert.ToDateTime("16/05/2020") });
            cards.Add(new CardInfo() { ID = 7, CardNo = "398903490853904", ExpiryDate = Convert.ToDateTime("03/02/2019") });
            cards.Add(new CardInfo() { ID = 8, CardNo = "4890890348503940", ExpiryDate = Convert.ToDateTime("02/02/2020") });
            cards.Add(new CardInfo() { ID = 9, CardNo = "4279238789473340", ExpiryDate = Convert.ToDateTime("20/02/2022") });
            cards.Add(new CardInfo() { ID = 10, CardNo = "5789023809234830", ExpiryDate = Convert.ToDateTime("02/02/2003") });
            cards.Add(new CardInfo() { ID = 11, CardNo = "6238902384029380", ExpiryDate = Convert.ToDateTime("15/02/2022") });
            cards.Add(new CardInfo() { ID = 12, CardNo = "7239028409839930", ExpiryDate = Convert.ToDateTime("05/03/2024") });
            cards.Add(new CardInfo() { ID = 13, CardNo = "8728379", ExpiryDate = Convert.ToDateTime("12/04/2020") });
            cards.Add(new CardInfo() { ID = 14, CardNo = "8390480349894380", ExpiryDate = Convert.ToDateTime("17/03/2010") });
            cards.Add(new CardInfo() { ID = 15, CardNo = "3940534958930450", ExpiryDate = Convert.ToDateTime("20/02/2010") });
            cards.Add(new CardInfo() { ID = 16, CardNo = "334934095090444", ExpiryDate = Convert.ToDateTime("10/06/2010") });
            cards.Add(new CardInfo() { ID = 17, CardNo = "3289290384908230", ExpiryDate = Convert.ToDateTime("20/02/2012") });
            cards.Add(new CardInfo() { ID = 18, CardNo = "4320829379847340", ExpiryDate = Convert.ToDateTime("10/04/2010") });
            cards.Add(new CardInfo() { ID = 19, CardNo = "5237890428309480", ExpiryDate = Convert.ToDateTime("20/02/2012") });
            cards.Add(new CardInfo() { ID = 20, CardNo = "123789042830942", ExpiryDate = Convert.ToDateTime("20/02/2012") });

            return cards;
        }
    }
}