

use Cards
go

truncate table CardInfo
insert into CardInfo values(1234567890123450, convert(datetime, '02/02/2020', 103))
insert into CardInfo values(1278934723430240, convert(datetime, '02/05/2023', 103))
insert into CardInfo values(2349087834984030, convert(datetime, '02/02/2024', 103))
insert into CardInfo values(3830489593048590, convert(datetime, '02/02/2026', 103))
insert into CardInfo values(3489483405830490, convert(datetime, '02/02/2020', 103))
insert into CardInfo values(378209831092292, convert(datetime, '16/05/2020', 103))
insert into CardInfo values(398903490853904, convert(datetime, '03/02/2019', 103))
insert into CardInfo values(4890890348503940, convert(datetime, '02/02/2020', 103))
insert into CardInfo values(4279238789473340, convert(datetime, '20/02/2022', 103))
insert into CardInfo values(5789023809234830, convert(datetime, '02/02/2003', 103))
insert into CardInfo values(6238902384029380, convert(datetime, '15/02/2022', 103))
insert into CardInfo values(7239028409839930, convert(datetime, '05/03/2024', 103))
insert into CardInfo values(8728379, convert(datetime, '12/04/2020', 103))
insert into CardInfo values(8390480349894380, convert(datetime, '17/03/2010', 103))
insert into CardInfo values(3940534958930450, convert(datetime, '20/02/2010', 103))
insert into CardInfo values(334934095090444, convert(datetime, '10/06/2010', 103))
insert into CardInfo values(3289290384908230, convert(datetime, '20/02/2012', 103))
insert into CardInfo values(4320829379847340, convert(datetime, '10/04/2010', 103))
insert into CardInfo values(5237890428309480, convert(datetime, '20/02/2012', 103))
insert into CardInfo values(123789042830942, convert(datetime, '20/02/2012', 103))

select * from CardInfo

