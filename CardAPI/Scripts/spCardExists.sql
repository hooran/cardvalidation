use [cards]
go

set ansi_nulls on
go
set quoted_identifier on
go

create procedure [dbo].[CardExists]
	@CardNo nvarchar(16)
as
begin
	set nocount on;
	select top 1 ExpiryDate from CardInfo where CardNo = @CardNo;
end
