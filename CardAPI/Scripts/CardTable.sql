use [Cards]
go

/****** object:  table [dbo].[cardinfo]    script date: 27/09/2018 07:20:33 ******/
set ansi_nulls on
go

set quoted_identifier on
go

create table [dbo].[CardInfo](
	[ID] [int] identity(1,1) not null,
	[CardNo] [nvarchar](16) not null,
	[ExpiryDate] [datetime] null,
 constraint [pk_CardInfo] primary key clustered 
(
	[ID] asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [primary]
) on [primary]

go

