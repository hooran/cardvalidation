﻿using CardAPI.Framework;
using CardAPI.Helper;
using CardAPI.Models;
using CardAPI.Repositories;
using System;
using System.Web.Http;

namespace CardAPI.Controllers
{
    public class ValidationController : ApiController
    {
        private IDataRepository dataRepository;
        public ValidationController(IDataRepository _dataRepository)
        {
            dataRepository = _dataRepository;
        }

        [HttpGet]
        public Card Get(string number)
        {
            try
            {
                var expiryDate = dataRepository.CardExists(number);
                var cardType = CardHelper.GetCardType(number);

                if (expiryDate == null) return new Card() { CardType = string.Empty, Validation = Constants.DoesNotExist };
                else if (cardType == null) return new Card() { CardType = string.Empty, Validation = Constants.Invalid };
                else
                {
                    var validation = CardHelper.Validate(number, Convert.ToDateTime(expiryDate));
                    return new Card() { CardType = cardType.ToString(), Validation = validation };
                }
            }

            catch (Exception ex)
            {
                Logger.Error(ex);
                throw ex;
            }
        }
    }
}
